<?php get_header(); ?>
	
	<div class="section content container">	
				
		<?php if ( have_posts() ) : ?>
		
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>

		<?php endif; ?>
		
	</div><!-- end .content -->

<?php get_footer(); ?>