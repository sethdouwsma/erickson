<!DOCTYPE HTML>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>

	<meta charset="utf-8">
	
	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php wp_title('|',true,'right'); ?></title>
	
	<!-- mobile meta (hooray!) -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no"/>
	
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="/favicon.ico">
	<![endif]-->
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css">
	
	<!--[if IE]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css" type="text/css" media="screen"><![endif]-->
	<!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<?php wp_head(); ?>
	
	<!-- drop Google Analytics Here -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-19082196-41', 'auto');
	  ga('send', 'pageview');
	
	</script>
	<!-- end analytics -->

</head>

<body <?php body_class(); ?>>

	<div class="opl"><a href="http://onepagelove.com/erickson-associates" target="_blank">Erickson & Associates</a></div>
	
	<div id="home" class="nav_wrapper">
		<div class="prenav">
			<div class="container clearfix">
				<p><span>Call:</span> 559-897-5824</p>
				<p><span>Email:</span> <a href="mailto:info@ericksoncpas.com">info@ericksoncpas.com</a></p>
			</div>
		</div>
	
		<div class="main_nav not_sticky">
			<div class="container clearfix">
				<div class="logo"><a href="#home">Erickson CPAs</a></div>
				<p class="header_toggle">&#9776;</p>
				<ul class="navigation">
		            <li><a href="#services">Services</a></li>
		            <li><a href="#about">About</a></li>
		            <li><a href="#team">Team</a></li>
		            <li><a href="#contact">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>