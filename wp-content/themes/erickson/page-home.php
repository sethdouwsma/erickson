<?php 
/* 
Template Name: Home
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	
	<header>
		<div class="container">
			<div class="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/header_logo.svg" alt="Erickson CPAs" /></div>
		</div>	
	</header><!-- end header -->
	
	<div id="tradition">
		<div class="container intro section">
			<h4><?php the_field('intro_header'); ?></h4>
			<hr>
			<?php the_field('intro'); ?>
			<p class="button dark"><a href="#contact" class="button-orange-stroked scroll_down">Let's Work Together</a></p>
		</div>
	</div>
	
	<div id="services" class="small_section">
		<div class="services_left">
			<div class="services_left_image"></div>
			<div class="services_left_text">
				<h5>Services</h5>
				<h3>We do more than number crunching</h3>
					<?php if( have_rows('service') ): ?>
						<?php $flag = 0; $service = 0; ?>
						<ol class="services_list nav">
							<?php while( have_rows('service') ): the_row(); ?>
								<li <?=$flag==0?"class='active'":""?> data-service="<?php echo ++$service ?>"><?php the_sub_field('name'); ?></li>
							<?php $flag = 1; endwhile; ?>
					    </ol>
					<?php endif; ?>
			</div>
		</div>
		
		<div class="services_right">
			<div class="service_wrapper clearfix">
			
				<?php if( have_rows('service') ): ?>
					<?php $flag = 0; $service = 0; ?>
					<?php while( have_rows('service') ): the_row(); ?>
						<div data-service="<?php echo ++$service ?>" class="view <?=$flag==0?"active":""?>">
							<div class="service_main">
								<p class="h5"><span><?php echo $service; ?>.</span> <?php the_sub_field('name'); ?></p>
								<?php the_sub_field('description'); ?>
							</div>
							<?php if( have_rows('service_highlights') ): ?>
								<div class="service_extra">
									<p class="h5">Service Highlights</p>
									<ul>
									<?php while( have_rows('service_highlights') ): the_row(); ?>
										<li><?php the_sub_field('highlight'); ?></li>
									<?php endwhile; ?>
									</ul>
								</div>
							<?php endif; ?>
						</div><!-- end .view -->
				
					<?php $flag = 1; endwhile; ?>
				<?php endif; ?>
			</div><!-- end service_wrapper -->
		</div>
	</div><!-- end #services -->
	
	<div id="about">
		<div class="container intro section">
			<h4><?php the_field('about_intro_header'); ?></h4>
			<hr>
			<?php the_field('about_intro'); ?>
		</div>
		
		<div class="container about_image"><p><img src="<?php echo get_template_directory_uri(); ?>/images/about_image.jpg" alt="Erickson CPAs" /></p></div>
		
		<div class="history">
			<div class="overlay"></div>
			<div class="container">
				<h5>About Erickson & Associates</h5>
				<h3><?php the_field('history_header'); ?></h3>
				<?php the_field('history'); ?>
			</div>
		</div>
	
		<div class="tl_wrapper section">
			<div class="tl_line"></div>
			
			<?php if( have_rows('timeline') ): ?>
				<ul class="tl container clearfix">
				<?php while( have_rows('timeline') ): the_row(); ?>
						
					<li class="tl_point">
						<div class="tl_info">
							<h5><?php the_sub_field('date'); ?></h5>
							<p><?php the_sub_field('description'); ?></p>
						</div>
					</li>
			
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
				
		</div>
	</div><!-- end #about -->
	
	<div id="team">
		
		<div class="team_intro">
			<div class="team_intro_image"></div>
			<div class="team_intro_text">
				<h5>The team at erickson & associates</h5>
				<h3>Gerald Erickson, CPA - owner</h3>
				<?php the_field('gerald'); ?>
			</div>
		</div>
		
		<?php if( have_rows('team') ): ?>
			<div class="container section">
			<ul class="team_buckets clearfix">
				<?php while( have_rows('team') ): the_row(); ?>
					<li class="team_bucket">
						<div class="team_bucket_image" style="background: url(<?php the_sub_field('image'); ?>) no-repeat center center"></div>
						<h4><?php the_sub_field('name');?></h4>
						<h5><?php the_sub_field('title');?></h5>
					</li>
				<?php endwhile; ?> 
			</ul>
			</div>
		<?php endif; ?>
	
	</div><!-- end #team -->
	
	<div id="values">
		<div class="values_divider">
			<div class="divider_logo_wrapper">
				<span class="padding"></span>
				<p class="divider_logo"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" /></p>
			</div>
		</div>
		<div class="container intro section">
			<h4><?php the_field('assist_header'); ?></h4>
			<hr>
			<?php the_field('assist_body'); ?>
		</div>
	</div><!-- end #values -->
	
	<div id="contact">
		<div class="map_wrap">
			<div id="map"></div>
		</div>
		<div class="contact_wrapper">
			<div class="container">
				<div class="contact_info">
					<h5>Erickson & Associates CPA</h5>
					<address>
						<p class="address"><?php the_field('address'); ?></p>
						<p>Tel: <?php the_field('telephone'); ?></p>
						<p>Fax: <?php the_field('fax'); ?></p>
						<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
					</address>
				</div>
				<div class="form_wrapper">
					<?php echo do_shortcode('[contact-form-7 id="39" title="Contact Form"]'); ?>
				</div>
			</div>
		</div>
	</div><!-- end #contact -->		
	
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>
