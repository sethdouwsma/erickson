jQuery(document).ready(function ($) {

	// Header toggle
	$('.header_toggle').click(function() {
		$('.navigation').toggleClass( "toggled" );
	});

	// Single Page Nav
	$('.navigation').singlePageNav({
	    offset: $('.main_nav').height()
	});
	
	$( '.scroll_down' ).click(function(e) {
		e.preventDefault();
		$('html, body').animate({
		    scrollTop: ($('#contact').first().offset().top)
		},400);
	});
	
	// Sticky Nav
	$(window).scroll(function(){
	     var top 	 = $(this).scrollTop();
		 var height	 = $('.prenav').height();
	     var height2 = $('.main_nav').outerHeight(true);
	     	 
	     if(top >= height) {
	         $('.main_nav').removeClass('not_sticky').addClass('sticky'); 
	         $('.prenav').css("padding-bottom", 10+height2);   
	     }
	     
	     if(top < height && $('.main_nav').hasClass('sticky')) {
	         $('.main_nav').removeClass('sticky').addClass('not_sticky');  
	         $('.prenav').css("padding-bottom", 10);     
	     }
	});
	
	$('#services .nav li').click(function() {
	    
	    var selected = $(this).data('service');
	    
	    if (!$('.view[data-service="' + selected + '"]').hasClass("active")) {
	    
	    	$('#services .nav').find('li.active').removeClass("active");
	    	$('.view.active').removeClass("active");
		    $('#services .nav li[data-service="' + selected + '"]').addClass("active");
		    $('.view[data-service="' + selected + '"]').addClass("active");
		    
	    } else {
	    
		    return false;
		    
	    }
	     
    });
	
	
});
