<?php
/**
 * The default template for displaying post content
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry_header">
		<div class="author_image"><?php echo get_avatar(get_the_author_meta( 'ID' )); ?></div>
		<h3 class="entry_title"><?php the_title(); ?></h3>
		<h4 class="entry_meta"><?php echo get_the_date(); ?> - <?php the_author_posts_link() ?> - <?php the_category(', ') ?></h4>
		<hr>
	</div>
	<div class="entry_content">		
		<?php the_content(); ?>
	</div>
</article>