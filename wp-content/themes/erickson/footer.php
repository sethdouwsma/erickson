<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 */
?>
				
	<footer>
		<div class="upper_footer">
			<div class="container">
				<div class="footer_logo"><img src="<?php echo get_template_directory_uri(); ?>/images/footer_logo.svg" alt="Erickson CPAs" /></div>
			</div>
		</div>
		<div class="lower_footer">
			<div class="container">
				<p class="copyright">&copy; <?php echo date("Y") ?> Erickson & Associates. All Rights Reserved.</p>
				<p class="credentials"><a href="http://edencreative.co" target="_blank">Site Crafted By Eden</a></p>
			</div>
		</div>
	</footer>

	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.singlePageNav.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
	
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>        
    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);
    
        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var myLatlng = new google.maps.LatLng(36.516145, -119.551807);
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 15,

                // The latitude and longitude to center the map (always required)
                center: myLatlng,
                scrollwheel: false,

                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [
						    {
						        "featureType": "water",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#d3d3d3"
						            }
						        ]
						    },
						    {
						        "featureType": "transit",
						        "stylers": [
						            {
						                "color": "#808080"
						            },
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#b3b3b3"
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#ffffff"
						            },
						            {
						                "weight": 1.8
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#d7d7d7"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#ebebeb"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "color": "#a7a7a7"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#ffffff"
						            }
						        ]
						    },
						    {
						        "featureType": "landscape",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#efefef"
						            }
						        ]
						    },
						    {
						        "featureType": "road",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "color": "#696969"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative",
						        "elementType": "labels.text.fill",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "color": "#737373"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "elementType": "labels",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "elementType": "geometry.stroke",
						        "stylers": [
						            {
						                "color": "#d6d6d6"
						            }
						        ]
						    },
						    {
						        "featureType": "road",
						        "elementType": "labels.icon",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {},
						    {
						        "featureType": "poi",
						        "elementType": "geometry.fill",
						        "stylers": [
						            {
						                "color": "#dadada"
						            }
						        ]
						    }
						]
            };

            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using out element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            
            var url = '<?php echo get_template_directory_uri(); ?>/images/map_marker.png';
			var size = new google.maps.Size(70, 80);
			if(window.devicePixelRatio > 1.5){
				url = '<?php echo get_template_directory_uri(); ?>/images/map_marker@2x.png';
				size = new google.maps.Size(140, 160);
			}
			var image = {
				url: url,
				size: size,
				scaledSize: new google.maps.Size(50, 57),
				origin: new google.maps.Point(0,0),
				anchor: new google.maps.Point(16, 37)
			};
			
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				icon: image
			});
        }
    </script>
	
	<?php wp_footer(); ?>
	
</body>
</html>
